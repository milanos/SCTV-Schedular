import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import FileForm from './form.js';
import Scheduler from './schedular.js';
import Basic from './sample.js'
// import Controls from './control.js';

class App extends Component {
  render() {
    return (
        <div>
        <FileForm />
        <Scheduler />
        </div>
    );
  }
}

export default App;

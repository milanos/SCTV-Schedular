const Data = {
    resources: [        
        { id : 'ex', name: 'EXAMPLE'},
        { id: "monday", name: "Monday" },
        { id: "tuesday", name: "Tuesday" },
        { id: "wednesday", name: "Wednesday" },
        { id: "thursday", name: "Thursday" },
        { id: "friday", name: "Friday" },
        { id: "satday", name: "Satday" },
        { id: "sunday", name: "Sunday" },
        // { id: '10', name: '1 AM' },
        // { id: '15', name: '1:30 AM' },
        // { id: '20', name: '2 AM' },
        // { id: '25', name: '2:30 AM' },
        // { id: '30', name: '3 AM' },
        // { id: '35', name: '3:30 AM' },
        // { id: '40', name: '4 AM' },
        // { id: '45', name: '4:30 AM' },
        // { id: '50', name: '5 AM' },
        // { id: '55', name: '5:30 AM' },
        // { id: '60', name: '6 AM' },
        // { id: '65', name: '6:30 AM' },
        // { id: '70', name: '7 AM' },
        // { id: '75', name: '7:30 AM' },
        // { id: '80', name: '8 AM' },
        // { id: '85', name: '8:30 AM' },
        // { id: '90', name: '9 AM' },
        // { id: '95', name: '9:30 AM' },
        // { id: '100', name: '10 AM' },
        // { id: '105', name: '10:30 AM' },
        // { id: '110', name: '11 AM' },
        // { id: '115', name: '11:30 AM' },
        // { id: '120', name: '12 AM' },
        // { id: '125', name: '12:30 AM' },
        // { id: '130', name: '1 PM' },
        // { id: '135', name: '1:30 PM' },
        // { id: '140', name: '2 PM' },
        // { id: '145', name: '2:30 PM' },
        // { id: '150', name: '3 PM' },
        // { id: '155', name: '3:30 PM' },
        // { id: '160', name: '4 PM' },
        // { id: '165', name: '4:30 PM' },
        // { id: '170', name: '5 PM' },
        // { id: '175', name: '5:30 PM' },
        // { id: '180', name: '6 PM' },
        // { id: '185', name: '6:30 PM' },
        // { id: '190', name: '7 PM' },
        // { id: '195', name: '7:30 PM' },
        // { id: '200', name: '8 PM' },
        // { id: '205', name: '8:30 PM' },
        // { id: '210', name: '9 PM' },
        // { id: '215', name: '9:30 PM' },
        // { id: '220', name: '10 PM' },
        // { id: '225', name: '10:30 PM' },
        // { id: '230', name: '11 PM' },
        // { id: '235', name: '11:30 PM' },
        // { id: '240', name: '12 PM' },
        // { id: '245', name: '12:30 PM' }

    ],

    events: [
        {
            id: 0,
            start: (new Date()).toISOString().substring(0,10) + " 00:00:00",
            end: (new Date()).toISOString().substring(0,10) + " 01:00:00",
            resourceId: "ex",
            title: "Example",
            bgColor: '#D9D9D9'
            
        }
        // {
        //     id: 1,
        //     start: '2017-12-18 09:30:00',
        //     end: '2018-05-19 23:30:00',
        //     resourceId: '10',
        //     title: 'I am finished',
        //     bgColor: '#D9D9D9'
        // },
        ],
    eventsForTaskView: [
        // {
        //     id: 1,
        //     start: '2017-12-18 09:30:00',
        //     end: '2017-12-18 23:30:00',
        //     resourceId: 'r1',
        //     title: 'I am finished',
        //     bgColor: '#D9D9D9',
        //     groupId: 1,
        //     groupName: 'Task1'
        // },
        ],
    eventsForCustomEventStyle: [
    //     {
    //         id: 1,
    //         start: '2017-12-18 09:30:00',
    //         end: '2017-12-19 23:30:00',
    //         resourceId: 'r1',
    //         title: 'I am finished',
    //         bgColor: '#D9D9D9',
    //         type: 1
    //     },
    ],
};

export default Data;

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
class FileForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            disc: '',
            fname: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        console.log(event);
        this.setState({
            title: event.target.title,
            disc: event.target.disc,
            fname: event.target.file
        });
    }

    handleSubmit(event) {
      event.preventDefault();
    }
    getTitle() {
        return this.state.title;
    }
    getDisc = () =>
        this.state.disc;

    getFname() {
        this.state.fname;
    }
    render() {
        return (

                <form onSubmit={this.handleSubmit}>

                <label>

                  <p>Title: {this.state.title}</p>
                  <p>Description: {this.state.disc}</p>
                  <p>File Name: {this.state.fname}</p>

                <div>Title:</div>
                <input type="text" value={this.state.title} onChange={this.handleChange} />
                </label>
                <label>
                <div>Disc:</div>
                <input type="text" value={this.state.disc} onChange={this.handleChange} />
                </label>
                <label>
                <div>File: </div>
                <input type="file" id="file" name="file" />
                </label>
                <div></div>
                <input type="submit" value="Submit" />
                </form>
        );
    }
}

export default FileForm;

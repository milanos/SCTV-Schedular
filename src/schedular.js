import React, {Component} from 'react';
import {PropTypes} from 'prop-types' ;
import Scheduler, {SchedulerData, ViewTypes, DATE_FORMAT } from 'react-big-scheduler';
import Data from './data.js';
import withDragDropContext from './withDnDContext';
import FileForm from './form.js';
var fs = require('fs');
var md5 = require("nodejs-md5");

class Manager extends Component{

    constructor(props){
        super(props);

        let schedulerData = new SchedulerData((new Date()).toISOString().substring(0,10), ViewTypes.Day);
        schedulerData.localeMoment.locale('en');
        schedulerData.setResources(Data.resources);
        schedulerData.setEvents(Data.events);
        this.state = {
            viewModel: schedulerData
        };
    }

    render(){
 const {viewModel} = this.state;

        let leftCustomHeader = (
            <div><span style={{fontWeight: 'bold'}}><a onClick={this.addResource}>Add a resource</a></span></div>
        );

        return (
            <div>
                <div>
                    <Scheduler schedulerData={viewModel}
                               prevClick={this.prevClick}
                               nextClick={this.nextClick}
                               onSelectDate={this.onSelectDate}
                               onViewChange={this.onViewChange}
                               eventItemClick={this.eventClicked}
                               viewEventClick={this.edit}
                               viewEventText="Edit"
                               viewEvent2Text="Delete"
                               viewEvent2Click={this.delete}
                               updateEventStart={this.updateEventStart}
                               updateEventEnd={this.updateEventEnd}
                               moveEvent={this.moveEvent}
                               newEvent={this.newEvent}
                    />
                </div>
            </div>
        );
    }
    readFile = (file) => {
        var rawFile = new XMLHttpRequest();
        var data;
        rawFile.open("GET", file, false);
        rawFile.onreadystatechange = function ()
        {

            if(rawFile.readyState === 4)
            {
                if(rawFile.status === 200 || rawFile.status === 0)
                {
                    var allText = rawFile.responseText;
                    data = allText;
                }
            }
        };
        return data;
        rawFile.send(null);
    }
    // currentDate = () => {
    //     var d = new Date;
    //     return d.getFullYear() + "-" + d.getMonths() +
    //         "" + d.getDate() + " " + d.getHours() +
    //         ":" + d.getMinutes() + ":" + d.getSeconds();
    // }

    // currentDatePlusHr = () => {
    //     var d = new Date;
    //     d.setHours(d.getHours + 1);
    //     return d.getFullYear() + "-" + d.getMonths() +
    //         "" + d.getDate() + " " + d.getHours() +
    //         ":" + d.getMinutes() + ":" + d.getSeconds();
    // }

    prevClick = (schedulerData)=> {
        schedulerData.prev();
        schedulerData.setEvents(Data.events);
        this.setState({
            viewModel: schedulerData
        });
    }

    nextClick = (schedulerData)=> {
        schedulerData.next();
        schedulerData.setEvents(Data.events);
        this.setState({
            viewModel: schedulerData
        });
    }

    onViewChange = (schedulerData, view) => {
        schedulerData.setViewType(view.viewType, view.showAgenda, view.isEventPerspective);
        schedulerData.setEvents(Data.events);
        this.setState({
            viewModel: schedulerData
        });
    }

    onSelectDate = (schedulerData, date) => {
        schedulerData.setDate(date);
        schedulerData.setEvents(Data.events);
        this.setState({
            viewModel: schedulerData
        });
    }

    eventClicked = (schedulerData, event) => {
        alert(`You just clicked an event: {id: ${event.id}, title: ${event.title}}`);
    };

    delete = (schedulerData, event) => {
        alert(`You just executed delete to event: {id: ${event.id}, title: ${event.title}}`);
        var e = schedulerData["events"].filter(i => JSON.stringify(i) !== JSON.stringify(event));
        schedulerData.setEvents(e);
        this.setState({
            viewModel: schedulerData
        });
    };

    edit = (schedulerData, event) => {
        console.log(schedulerData);

        console.log(schedulerData["events"]);
        console.log(event);
        console.log(FileForm.getTitle());
    };
    getDuration = (fname) => {
        var buff = new Buffer(100);
        fs.open(fname, 'r', function(err, fd) {
            fs.read(fd, buff, 0, 100, 0, function(err, bytesRead, buffer) {
                var start = buffer.indexOf(new Buffer('mvhd')) + 17;
                // var timeScale = buffer.readUInt32BE(start, 4);
                var duration = buffer.readUInt32BE(start + 4, 4);
                // var movieLength = Math.floor(duration/timeScale);
                return duration;

            });
        });
    }


    // idManagement = () => {
    //     if(FileForm)
    // }
    getTitle = () => {
        if (FileForm.getTitle() == 0){
            return "Title";
        } else {
            return FileForm.getTitle();
        }

    }
    contentsPath = () => "../sctv/vids/"
newEvent = (schedulerData, slotId, slotName, start, end, type, item) => {
            let newFreshId = 0;
            schedulerData.events.forEach((item) => {
                if(item.id >= newFreshId)
                    newFreshId = item.id + 1;
            });

            let newEvent = {
                id: newFreshId,
                title: "title",
                start: start,
                end: end,
                resourceId: slotId,
                bgColor: 'purple'
            };
            schedulerData.addEvent(newEvent);
            this.setState({
                viewModel: schedulerData
            });
}
    updateEventStart = (schedulerData, event, newStart) => {
        schedulerData.updateEventStart(event, newStart);
        this.setState({
            viewModel: schedulerData
        });
    }

    updateEventEnd = (schedulerData, event, newEnd) => {
        schedulerData.updateEventEnd(event, newEnd);
        this.setState({
            viewModel: schedulerData
        });
    }

    moveEvent = (schedulerData, event, slotId, slotName, start, end) => {
        schedulerData.moveEvent(event, slotId, slotName, start, end);
        this.setState({
            viewModel: schedulerData
        });
    }
}

export default withDragDropContext(Manager);
